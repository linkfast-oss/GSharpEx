﻿using System;
using System.Collections.Generic;

namespace GSharpEx.Toolbox
{
    public class CmdArgumentsBuilder
    {

#if DEBUG
		private void Debug(String data) { Console.WriteLine("CmdArgumentsBuilder: " + data); }
#else
		private void Debug(String data) {}
#endif

		public List<string> Arguments { get; }
        string DefaultConnector = " ";

        public CmdArgumentsBuilder(string defaultConnector=" ")
        {
            Arguments = new List<string>();
            DefaultConnector = defaultConnector;

			Debug("Constructor");
		}

        public CmdArgumentsBuilder AddOption(string option, string value, string connector=null) {
            if (connector == null)
            {
                connector = DefaultConnector;
            }

            Debug(String.Format("{0}{1}{2}", option, connector, value));

			Arguments.Add(String.Format("{0}{1}{2}", option, connector, value));
            return this;
        }

        public CmdArgumentsBuilder Add(string argument) {
            Arguments.Add(argument);

			Debug(argument);

			return this;
        }

        public CmdArgumentsBuilder Add(List<string> argumentList)
        {
            foreach (var argument in argumentList) {
				Debug(argument);
				Arguments.Add(argument);
            }
            return this;
        }

        public CmdArgumentsBuilder AddDoubleDashed(Dictionary<string, string> argumentDictionary, string connector=null) {
            if (connector == null)
            {
                connector = DefaultConnector;
            }
            foreach (var kv in argumentDictionary) {
				
				Debug(kv.Key + " " + kv.Value);

				Arguments.Add(String.Format("--{0}{1}{2}", kv.Key, connector, kv.Value));
            }
            return this;
            
        }

        public string Chain() {
            return ToString();
        }

        override public string ToString() {
            string chain = "";
            foreach (var arg in Arguments) {
				Debug(arg);

				chain += String.Format("{0} ", arg);
            }
            return chain.Substring(0, chain.Length-1);
        }
    }
}
