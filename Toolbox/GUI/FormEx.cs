﻿using System;
using Gtk;
namespace GSharpEx.Toolbox.GUI
{
    public static class FormEx
    {        
		public static void MessageBox(Gtk.Window parent, String title, String text)
        {
			MessageBox(parent, title, text, MessageType.Info);
        }

		public static void OkMessageBox(Gtk.Window parent, String title, String text, MessageType msgType)
        {
			MessageDialog md = new MessageDialog(parent,
                    DialogFlags.DestroyWithParent, msgType,
			                                     ButtonsType.Ok, text);
			md.WindowPosition = WindowPosition.Center;
            md.Title = title;
            md.Run();
            md.Destroy();
        }

		public static void MessageBox(Gtk.Window parent, String title, String text, MessageType msgType)
        {
            MessageDialog md = new MessageDialog(parent,
			        DialogFlags.DestroyWithParent, msgType,
                    ButtonsType.Close, text);
			md.WindowPosition = WindowPosition.Center;
            md.Title = title;
            md.Run();
            md.Destroy();
        }

        public static bool YesNoMessageBox(Gtk.Window parent, String title, String text, MessageType msgType)
        {
            MessageDialog md = new MessageDialog(parent,
                    DialogFlags.DestroyWithParent, msgType,
                                                 ButtonsType.YesNo, text);
			md.WindowPosition = WindowPosition.Center;
            md.Title = title;
            ResponseType rt = (ResponseType)md.Run();
            md.Destroy();

            return rt == ResponseType.Yes;
        }
    }
}
