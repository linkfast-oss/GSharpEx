﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Principal;

namespace GSharpEx.Toolbox
{
    public class SystemEx
    {
		public enum OS {
			macOS, Windows, Linux
		}

		public static OS GetOS()
        {
            OperatingSystem os = Environment.OSVersion;
            PlatformID pid = os.Platform;
            if (pid == PlatformID.Unix || pid == PlatformID.MacOSX)
            {
                String outputFromUname = Execution.RunExternalExe("uname", "-a");
				if (outputFromUname.Contains("Darwin")) return OS.macOS;
				else return OS.Linux;
            }
            else
            {
				return OS.Windows;
            }
        }

		public static string GetOSCoreVersion() {
			switch (GetOS()) {
				case OS.Linux:
					return "14.04";
				default:
					return "0.0";
			}
		}

		[System.Runtime.InteropServices.DllImport("kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode, SetLastError = true)]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        static extern bool SetDllDirectory(string lpPathName);

        public static bool CheckWindowsGtk(string ApplicationName)
        {
            string location = null;
            Version version = null;
            Version minVersion = new Version(2, 12, 30);
            using (var key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Xamarin\GtkSharp\InstallFolder"))
            {
                if (key != null)
                    location = key.GetValue(null) as string;
            }
            using (var key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Xamarin\GtkSharp\Version"))
            {
                if (key != null)
                    Version.TryParse(key.GetValue(null) as string, out version);
            }
            if (location != null)
                Console.WriteLine("GTK+ Location: " + location);
            if (version == null || version != minVersion || location == null || !File.Exists(Path.Combine(location, "bin", "libgtk-win32-2.0-0.dll")))
            {
                //string url = "http://monodevelop.com/Download";
                string url = "https://dl.xamarin.com/GTKforWindows/Windows/gtk-sharp-2.12.30.msi";
                string caption = "Error de dependencias";
                string versionInfo = "";
                if (version != null)
                    versionInfo = (version > minVersion || version < minVersion) ? Environment.NewLine + Environment.NewLine + 
                                                                                              "Nota: Debe desinstalar la versión instalada actualmente primero." : "";
                string message =
                    "{0} no pudo encontrar la versión requerida de GTK# (2.12.30). Presione OK para iniciar la descarga." + versionInfo;
                if (DisplayWindowsOkCancelMessage(
                    string.Format(message, ApplicationName, url), caption)
                )
                {
                    Process.Start(url);
                }
                return false;
            }
            var path = Path.Combine(location, @"bin");
            try
            {
                if (SetDllDirectory(path))
                {
                    Console.WriteLine("0- SetDllDirectory: OK");
                    return true;
                } else {
                    Console.WriteLine("1- No se pudo establecer el directorio de DLL.");
                }
            }
            catch (EntryPointNotFoundException)
            {
                
            }
            Console.WriteLine("2- No se pudo establecer el directorio de DLL.");
            return true;
        }

        static bool DisplayWindowsOkCancelMessage(string message, string caption)
        {
            var name = typeof(int).Assembly.FullName.Replace("mscorlib", "System.Windows.Forms");
            var asm = Assembly.Load(name);
            var md = asm.GetType("System.Windows.Forms.MessageBox");
            var mbb = asm.GetType("System.Windows.Forms.MessageBoxButtons");
            var okCancel = Enum.ToObject(mbb, 1);
            var dr = asm.GetType("System.Windows.Forms.DialogResult");
            var ok = Enum.ToObject(dr, 1);
            const BindingFlags flags = BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Static;
            return md.InvokeMember("Show", flags, null, null, new object[] { message, caption, okCancel }).Equals(ok);
        }

		public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

		public static string GetFullPath(string fileName)
        {
            if (File.Exists(fileName))
                return Path.GetFullPath(fileName);

            var values = Environment.GetEnvironmentVariable("PATH");
            foreach (var path in values.Split(';'))
            {
                var fullPath = Path.Combine(path, fileName);
				//Console.WriteLine(fullPath);
                if (File.Exists(fullPath))
                    return fullPath;
            }
            return null;
        }

        public static string GetMagicNumbers(string filepath, int bytesCount)
        {
            byte[] buffer;
            using (var fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            using (var reader = new BinaryReader(fs))
                buffer = reader.ReadBytes(bytesCount);

            var hex = BitConverter.ToString(buffer);
            return hex.Replace("-", String.Empty).ToLower();
        }
        public static void ReplaceBytes(string filename, int position, byte[] data)
        {
            using (Stream stream = File.Open(filename, FileMode.Open))
            {
                stream.Position = position;
                stream.Write(data, 0, data.Length);
            }
        }
        public static void ReplaceBytes(string filename, int position, string hexData)
        {
            byte[] data = HexToBytes(hexData);
            ReplaceBytes(filename, position, data);
        }
        public static byte[] HexToBytes(string str)
        {
            if (str.Length == 0 || str.Length % 2 != 0)
                return new byte[0];

            byte[] buffer = new byte[str.Length / 2];
            char c;
            for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                // Convert first half of byte
                c = str[sx];
                buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);

                // Convert second half of byte
                c = str[++sx];
                buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
            }

            return buffer;
        }
    }
}
